/* Taken from https://github.com/djpohly/dwl/issues/466 */
#define COLOR(hex)    { ((hex >> 24) & 0xFF) / 255.0f, \
                        ((hex >> 16) & 0xFF) / 255.0f, \
                        ((hex >> 8) & 0xFF) / 255.0f, \
                        (hex & 0xFF) / 255.0f }
/* appearance */
static const int sloppyfocus               = 0;  /* focus follows mouse */
static const int bypass_surface_visibility = 0;  /* 1 means idle inhibitors will disable idle tracking even if it's surface isn't visible  */
static const unsigned int borderpx         = 3;  /* border pixel of windows */
static const unsigned int gappx            = 10; /* gap between windows */
static const float bordercolor[]           = COLOR(0x7f7f7fff);
static const float focuscolor[]            = COLOR(0x00ffffff);
static const float urgentcolor[]           = COLOR(0xff0000ff);
/* To conform the xdg-protocol, set the alpha to zero to restore the old behavior */
static const float fullscreen_bg[]         = {0.1, 0.1, 0.1, 1.0}; /* You can also use glsl colors */

/* tagging - TAGCOUNT must be no greater than 31 */
#define TAGCOUNT (9)

/* logging */
static int log_level = WLR_ERROR;

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating   monitor */
	/* examples:
	{ "Gimp",     NULL,       0,            1,           -1 },
	*/
	{ NULL,       NULL,       0,            0,           -1 },
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect                x    y */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL,   -1,  -1 },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
	.layout = "us-nocaps",
	.options = NULL,
};

static const int repeat_rate = 25;
static const int repeat_delay = 600;

/* Trackpad */
static const int tap_to_click = 0;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;
/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;

/* You can choose between:
LIBINPUT_CONFIG_CLICK_METHOD_NONE
LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS
LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER
*/
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;

/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;
/* You can choose between:
LIBINPUT_CONFIG_TAP_MAP_LRM -- 1/2/3 finger tap maps to left/right/middle
LIBINPUT_CONFIG_TAP_MAP_LMR -- 1/2/3 finger tap maps to left/middle/right
*/
static const enum libinput_config_tap_button_map button_map = LIBINPUT_CONFIG_TAP_MAP_LRM;

/* If you want to use the windows key for MODKEY, use WLR_MODIFIER_LOGO */
#define MODKEY WLR_MODIFIER_LOGO

#define TAGKEYS(KEY,SKEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,SKEY,toggletag, {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[] = { "footclient", NULL };
static const char *menucmd[] = { "tofi-drun", NULL };
static const char *fmcmd[] = { "footclient", "lf", NULL };
static const char *browsercmd[] = { "chromium", NULL };
static const char *addentrycmd[] = { "addentry", NULL };
static const char *viewjournalcmd[] = { "footclient", "viewjournal", NULL };
static const char *faketypecmd[] = { "faketype", NULL };
static const char *usercmd[] = { "userrbw", NULL };
static const char *passcmd[] = { "passrbw", NULL };
static const char *userpasscmd[] = { "userpassrbw", NULL };
static const char *sscopycmd[] = { "sscopy", NULL };
static const char *sscopyeditcmd[] = { "sscopyedit", NULL };
static const char *saveclipcmd[] = { "saveclip", NULL };
static const char *uairtogglecmd[] = { "uairctl", "toggle", NULL };
static const char *uairnextcmd[] = { "uairctl", "next", NULL };
static const char *popnotifcmd[] = { "fnottctl", "dismiss", NULL };
static const char *lockcmd[] = { "swaylock", "-c", "000000", NULL };
static const char *volupcmd[] = { "wpctl", "set-volume", "@DEFAULT_SINK@", "5%+", NULL };
static const char *voldowncmd[] = { "wpctl", "set-volume", "@DEFAULT_SINK@", "5%-", NULL };
static const char *volmutecmd[] = { "wpctl", "set-mute", "@DEFAULT_SINK@", "toggle", NULL };
static const char *mpdtogglecmd[] = { "mpc", "toggle", NULL };
static const char *mpdnextcmd[] = { "mpc", "next", NULL };
static const char *mpdprevcmd[] = { "mpc", "prev", NULL };
static const char *brupcmd[] = { "brightnessctl", "set", "5%+", NULL };
static const char *brdowncmd[] = { "brightnessctl", "set", "5%-", NULL };
static const char *blupcmd[] = { "brightnessctl", "-d", "smc::kbd_backlight", "set", "5%+", NULL };
static const char *bldowncmd[] = { "brightnessctl", "-d", "smc::kbd_backlight", "set", "5%-", NULL };
static const char *sdrbcmd[] = { "sdrb", NULL };

static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
	{ MODKEY,                    XKB_KEY_d,          spawn,            {.v = menucmd} },
	{ MODKEY,                    XKB_KEY_n,          spawn,            {.v = termcmd} },
	{ MODKEY,                    XKB_KEY_b,          spawn,            {.v = fmcmd} },
	{ MODKEY,                    XKB_KEY_w,          spawn,            {.v = browsercmd} },
	{ MODKEY,                    XKB_KEY_g,          spawn,            {.v = addentrycmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_G,          spawn,            {.v = viewjournalcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_T,          spawn,            {.v = faketypecmd} },
	{ MODKEY,                    XKB_KEY_bracketleft,spawn,            {.v = usercmd} },
	{ MODKEY,                    XKB_KEY_p,          spawn,            {.v = passcmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_P,          spawn,            {.v = userpasscmd} },
	{ MODKEY,                    XKB_KEY_s,          spawn,            {.v = sscopycmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_S,          spawn,            {.v = sscopyeditcmd} },
	{ MODKEY,                    XKB_KEY_a,          spawn,            {.v = saveclipcmd} },
	{ MODKEY,                    XKB_KEY_e,          spawn,            {.v = uairtogglecmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_E,          spawn,            {.v = uairnextcmd} },
	{ MODKEY,                    XKB_KEY_c,          spawn,            {.v = popnotifcmd} },
	{ MODKEY,                    XKB_KEY_x,          spawn,            {.v = lockcmd} },
	{ 0,      XKB_KEY_XF86AudioRaiseVolume,          spawn,            {.v = volupcmd} },
	{ 0,      XKB_KEY_XF86AudioLowerVolume,          spawn,            {.v = voldowncmd} },
	{ 0,      XKB_KEY_XF86AudioMute,                 spawn,            {.v = volmutecmd} },
	{ 0,      XKB_KEY_XF86AudioPlay,                 spawn,            {.v = mpdtogglecmd} },
	{ 0,      XKB_KEY_XF86AudioNext,                 spawn,            {.v = mpdnextcmd} },
	{ 0,      XKB_KEY_XF86AudioPrev,                 spawn,            {.v = mpdprevcmd} },
	{ 0,      XKB_KEY_XF86MonBrightnessUp,           spawn,            {.v = brupcmd} },
	{ 0,      XKB_KEY_XF86MonBrightnessDown,         spawn,            {.v = brdowncmd} },
	{ 0,      XKB_KEY_XF86KbdBrightnessUp,           spawn,            {.v = blupcmd} },
	{ 0,      XKB_KEY_XF86KbdBrightnessDown,         spawn,            {.v = bldowncmd} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_BackSpace,  spawn,            {.v = sdrbcmd } },
	{ MODKEY,                    XKB_KEY_j,          focusstack,       {.i = +1} },
	{ MODKEY,                    XKB_KEY_k,          focusstack,       {.i = -1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_H,          incnmaster,       {.i = +1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_L,          incnmaster,       {.i = -1} },
	{ MODKEY,                    XKB_KEY_h,          setmfact,         {.f = -0.05} },
	{ MODKEY,                    XKB_KEY_l,          setmfact,         {.f = +0.05} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_N,          zoom,             {0} },
	{ MODKEY,                    XKB_KEY_Tab,        view,             {0} },
	{ MODKEY,                    XKB_KEY_q,          killclient,       {0} },
	{ MODKEY,                    XKB_KEY_i,          setlayout,        {.v = &layouts[0]} },
	{ MODKEY,                    XKB_KEY_u,          setlayout,        {.v = &layouts[1]} },
	{ MODKEY,                    XKB_KEY_m,          setlayout,        {.v = &layouts[2]} },
	{ MODKEY,                    XKB_KEY_space,      setlayout,        {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_space,      togglefloating,   {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_F,          togglefullscreen, {0} },
	{ MODKEY,                    XKB_KEY_0,          view,             {.ui = ~0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_parenright, tag,              {.ui = ~0} },
	{ MODKEY,                    XKB_KEY_comma,      focusmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    XKB_KEY_period,     focusmon,         {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_less,       tagmon,           {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_greater,    tagmon,           {.i = WLR_DIRECTION_RIGHT} },
	TAGKEYS(          XKB_KEY_1, XKB_KEY_exclam,                     0),
	TAGKEYS(          XKB_KEY_2, XKB_KEY_at,                         1),
	TAGKEYS(          XKB_KEY_3, XKB_KEY_numbersign,                 2),
	TAGKEYS(          XKB_KEY_4, XKB_KEY_dollar,                     3),
	TAGKEYS(          XKB_KEY_5, XKB_KEY_percent,                    4),
	TAGKEYS(          XKB_KEY_6, XKB_KEY_asciicircum,                5),
	TAGKEYS(          XKB_KEY_7, XKB_KEY_ampersand,                  6),
	TAGKEYS(          XKB_KEY_8, XKB_KEY_asterisk,                   7),
	TAGKEYS(          XKB_KEY_9, XKB_KEY_parenleft,                  8),
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Q,          quit,           {0} },

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_Terminate_Server, quit, {0} },
	/* Ctrl-Alt-Fx is used to switch to another VT, if you don't know what a VT is
	 * do not remove them.
	 */
#define CHVT(n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
